# Guide d'initiation au SNDS
<!-- SPDX-License-Identifier: MPL-2.0 -->

Ce guide d'initiation au SNDS est à destination de tout nouvel utilisateur du SNDS. N'hésitez pas à faire des retours pour l'améliorer et l'enrichir via Gitlab ou le forum SNDS.

::: tip Crédits
Guide rédigé en 2022 pour le HDH via un marché public avec HEVA.
:::
