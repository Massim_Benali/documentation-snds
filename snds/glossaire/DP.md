# DP - Diagnostic principal
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le diagnostic principal est le motif des soins qui justifient l’hospitalisation. 

Il commande l’orientation dans une catégorie majeure de diagnostic au moment du groupage, et peut influer sur le classement dans un groupe homogène de malades (<link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />).

Avant la version 11 de la classification des GHM en <link-previewer href="MCO.html" text="MCO" preview-title="MCO - Médecine, chirurgie, obstétrique" preview-text="Terme utilisé pour désigner les activités aigus de courte durée réalisées dans les établissements de santé, en hospitalisation (avec ou sans hébergement) ou en consultations externes." />, il correspondait aux soins ayant mobilisé l’essentiel de l’effort médical et soignant. 


# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/diagnostic-principal-dp) sur le site internet du ministère
