# Retraite complémentaire des salariés 
<!-- SPDX-License-Identifier: MPL-2.0 -->

La retraite complémentaire des salariés est une retraite qui complète la retraite de base de la <link-previewer href="Cnav.html" text="CNAV" preview-title="Cnav - Caisse nationale d'assurance vieillesse" preview-text="La Caisse nationale d'assurance vieillesse (Cnav) gère la retraite des salariés, hors secteur agricole et hors fonction publique." />.
 
Cette retraite est gérée pour les salariés du privé par l'AGIRC-ARRCO, résultat de la fusion début 2019 de : 
- l’AGIRC (Association générale des institutions de retraite des **cadres**). 
- l’ARRCO (Association pour le régime de retraite complémentaire des **salariés**)

Les salariés non titulaires de la fonction publique, eux, cotisent à l'Ircantec.

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Retraite_compl%C3%A9mentaire_des_salari%C3%A9s_(France))
