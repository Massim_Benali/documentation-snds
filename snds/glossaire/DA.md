# DA - Diagnostic associé
<!-- SPDX-License-Identifier: MPL-2.0 -->

Un diagnostic associé est un diagnostic ou une pathologie majorant l’effort de soins et les moyens utilisés, par rapport à la morbidité principale. Les DA influencent le groupage des séjours dans un <link-previewer href="GHM.html" text="GHM" preview-title="GHM - Groupe homogène de malades" preview-text="Un groupe homogène de malades regroupe les prises en charge de même nature médicale et économique et constitue la catégorie élémentaire de classification en MCO. " />.

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/diagnostic-associe-da) sur le site internet du ministère
