# Traitements préliminaires des tables du DCIR
<!-- SPDX-License-Identifier: MPL-2.0 -->

Les tables du DCIR font l’objet d’un traitement préliminaire détaillé dans cette section. La version “transformée” est de la forme _ER_XXX_F. 

Les tables IR_BEN_R et IR_IMB_R sont également concernées et deviennent respectivement _IR_BEN_R et _IR_IMB_R.

::: tip Accéder au code source
Le code source est disponible sur le dépôt Gitlab [suivant](https://gitlab.com/healthdatahub/snds_omop)
:::
